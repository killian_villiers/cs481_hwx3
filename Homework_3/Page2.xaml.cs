﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Homework_3
{
    public partial class Page2 : ContentPage
    {
        public Page2()
        {
            InitializeComponent();
        }
        /*Function to handle push event to Page 3*/
        void Button_Clicked(System.Object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new Page3());
        }

        /*Function to handle pop event to go back on the navigation stack*/
        void Button_Clicked_1(System.Object sender, System.EventArgs e)
        {
            Navigation.PopAsync();
        }
    }
}
